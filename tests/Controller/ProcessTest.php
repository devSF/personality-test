<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProcessTest extends WebTestCase
{
    public function testProcess(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/test/process');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Are you an introvert or an extrovert?');

        $client->xmlHttpRequest('POST', '/test/process', [
            'current' => 0,
            'step' => 1,
            'answer' => '1'
        ]);
        $this->assertSelectorTextContains('span', 'Question 2/3');

        $client->xmlHttpRequest('POST', '/test/process', [
            'current' => 1,
            'step' => -1,
            'answer' => '0'
        ]);
        $this->assertSelectorTextContains('span', 'Question 1/3');

        $client->xmlHttpRequest('POST', '/test/process', [
            'current' => 1,
            'step' => 1,
            'answer' => '3'
        ]);
        $this->assertSelectorTextContains('span', 'Question 3/3');

        $client->xmlHttpRequest('POST', '/test/process', [
            'current' => 2,
            'step' => 1,
            'answer' => '2'
        ]);

        $crawler = $client->request('GET', '/test/result');

        $link = $crawler->selectLink('Take Another Test')->link();
        $client->click($link);

        $this->assertRouteSame('test_index');
    }
}

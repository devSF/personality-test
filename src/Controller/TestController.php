<?php

namespace App\Controller;

use App\Service\PersonalityResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/test', name: 'test_')]
class TestController extends AbstractController
{
    private array $questions = [];
    private array $questionsChoices = [];

    public function __construct()
    {
        $this->questions = [
            "You’re really busy at work and a colleague is telling you their life story and personal woes. You:",
            "You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:",
            "You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You"
        ];
        $this->questionsChoices = [[
            "Don’t dare to interrupt them",
            "Think it’s more important to give them some of your time; work can wait",
            "Listen, but with only with half an ear",
            "Interrupt and explain that you are really busy at the moment",
            ], [
            "Look at your watch every two minutes",
            "Bubble with inner anger, but keep quiet",
            "Explain to other equally impatient people in the room that the doctor is always running late",
            "Complain in a loud voice, while tapping your foot impatiently",
            ], [
            "Don’t dare contradict them",
            "Think that they are obviously right",
            "Defend your own point of view, tooth and nail",
            "Continuously interrupt your colleague",
        ]];
    }

    #[Route('', name: 'index')]
    public function index(): Response
    {
        return $this->render('test/index.html.twig');
    }

    #[Route('/process', name: 'process')]
    public function process(Request $request): Response
    {
        $session = $request->getSession();

        if(!$request->isXmlHttpRequest()) {
            $key =  $session->get('question_key') ?? 0;

            return $this->render('test/process.html.twig', [
                'key' => $key,
                'question' => $this->questions[$key],
                'question_choices' => $this->questionsChoices[$key],
            ]);
        }

        $answers = $session->get('answers');

        $key = $request->request->get('current') ?? $session->get('question_key') ?? 0;
        $step = $request->request->get('step') ?? 0;
        $answers[$key] = $request->request->get('answer') ?? 0;
        $session->set('answers', $answers);

        if(2 != $step) {
            $key += $step;
            $session->set('question_key', $key);
        }
        else {
            return new Response(-1);
        }

        return $this->render('test/process_content.html.twig', [
            'key' => $key,
            'question' => $this->questions[$key],
            'question_choices' => $this->questionsChoices[$key],
        ]);
    }

    #[Route('/result', name: 'result')]
    public function result(Session $session, PersonalityResolver $personalityResolver): Response
    {
        $answers = $session->get('answers');

        $result = $personalityResolver->processTrait($answers);

        $session->remove('question_key');
        $session->remove('answers');

        return $this->render('test/result.html.twig', ['result' => $result]);
    }

}

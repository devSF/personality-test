<?php

namespace App\Service;

class PersonalityResolver
{
    public function processTrait(array $answers): string
    {
        $scoreIntro = $scoreExtro = 0;

        foreach ($answers as $i => $answer) {

            $introChoices = match ($i) {
                0 => [1,3],
                1 => [0,2],
                2 => [2,3],
            };

            if(in_array(($answer-1), $introChoices)) {
                $scoreIntro++;
            }
            else {
                $scoreExtro++;
            }
        }

        return $scoreIntro > $scoreExtro ? 'Introvert' : 'Extrovert';
    }

}